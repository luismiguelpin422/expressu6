//Const para cuando la variable no va a cambiar
var app = require('./app');
//puerto en donde se va a estar ejecutando la variable
var port = 4000;

app.listen(port, () =>{
    console.log("Server ready");
});
