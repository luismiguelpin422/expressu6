var express = require('express');
var bodyParser = require('body-parser');

let app = express();
app.use( express.json());
app.use( express.urlencoded( {extended:true}) );

app.use( (req, res, next) =>{
    //todas que tengan ese encabezado       //'*' que puedo acceder de cualquier ruta
    res.header('Access-Control-Allow-Origin', '*');
    //que permita esos encabezados
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, XRequested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    //que permita los metodos 
    res.header('Access-Control-Allow-Methods','GET, POST, PUT, DELETE, OPTIONS');
    res.header('Allow', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});

module.exports = app;